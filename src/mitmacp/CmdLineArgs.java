/*
 * COMP90043 Cryptography and Security
 * Semester 2, 2016
 * Research Project: MITM Attack Demonstration
 * 
 * Author: Skye Rajmon McLeman
 * 
 * Date: 29 Oct, 2016
 * 
 * File: CmdLineArgs.java
 */
package mitmacp;

import org.kohsuke.args4j.Option;

//This class stores the arguments read from the command line.
public class CmdLineArgs {
	
	@Option(required = true, name = "-i", aliases = {"--interface"}, usage = "Network interface")
	private String interfaceName;
	
	@Option(required = true, name = "-t1", aliases = {"--target1"}, usage = "Target 1's IP address")
	private String ipTarget1;  //Path to text file containing configuration of servers
	
	@Option(required = true, name = "-t2", aliases = {"--target2"}, usage = "Target 2's IP address")
	private String ipTarget2;
	
	@Option(required = true, name = "-r", aliases = {"--repoison"}, usage = "Repoisoning interval (ms)")
	private String repoisonInterval;
	
	public String getInterfaceName() {
		return interfaceName;
	}

	public String getIpTarget1() {
		return ipTarget1;
	}
	
	public String getIpTarget2() {
		return ipTarget2;
	}
	
	public String getRepoisonInterval() {
		return repoisonInterval;
	}
}