/*
 * COMP90043 Cryptography and Security
 * Semester 2, 2016
 * Research Project: MITM Attack Demonstration
 * 
 * Author: Skye Rajmon McLeman
 * 
 * Date: 29 Oct, 2016
 * 
 * Description: This program can be used to implement a simple man-in-the-middle attack on a local 
 * area network via ARP cache poisoning. The Pcap4J library is used to create and send forged ARP 
 * reply frames containing the user's Ethernet address to the two targets specified. If packet 
 * forwarding is enabled, this will result in all of the traffic between the two targets being 
 * passed through the user's machine. By default the program will capture all such TCP traffic
 * and print the packets to stdout.
 * 
 * Note that the victims are repoisoned at the specified interval in order to maintain the attack
 * if the forged cache entries expire, or if any additional legitimate ARP requests/replies are
 * sent. 
 * 
 * File: MitmArpPoisonApplication.java
 */
package mitmacp;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

/* Driver for the program. Simply parses the given command line arguments, instantiates an
 * ArpPoisoner object using these arguments, and calls the run() method to commence the
 * attack.
 */
public class MitmArpPoisonApplication {
	public static void main(String[] args) {
		//Parse command line arguments
		CmdLineArgs argsBean = new CmdLineArgs();
		CmdLineParser cmdLineParser = new CmdLineParser(argsBean);
		try {
			cmdLineParser.parseArgument(args);
		} catch (CmdLineException e) {
			cmdLineParser.printUsage(System.err);
			System.exit(1);
		}
		
		MitmArpPoisoner ap = new MitmArpPoisoner(argsBean.getInterfaceName(),
										 argsBean.getIpTarget1(),
										 argsBean.getIpTarget2(),
										 argsBean.getRepoisonInterval());
		ap.run();
	}
}
